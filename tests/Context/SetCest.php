<?php

namespace ServiceCore\Version\Test\Context;

use Laminas\Http\Response;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;
use ServiceCore\Version\Context\Set;

/**
 * @group version
 */
class SetCest extends TestCase
{
    public function testConstruct(): void
    {
        $header  = 'foo';
        $version = 1;
        $service = new Set($header, $version);

        $reflectedHeader  = new ReflectionProperty($service, 'header');
        $reflectedVersion = new ReflectionProperty($service, 'version');

        $reflectedHeader->setAccessible(true);
        $reflectedVersion->setAccessible(true);

        $this->assertEquals($header, $reflectedHeader->getValue($service));
        $this->assertEquals($version, $reflectedVersion->getValue($service));
    }

    public function testSetVersion(): void
    {
        $header   = 'foo';
        $version  = 1;
        $service  = new Set($header, $version);
        $response = new Response();

        $service->setVersion($response);

        $this->assertTrue($response->getHeaders()->has($header));
    }
}
