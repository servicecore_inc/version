<?php

namespace ServiceCore\Version\Test\Context\Factory;

use Laminas\ServiceManager\ServiceManager;
use OutOfBoundsException;
use PHPUnit\Framework\TestCase;
use ServiceCore\Version\Context\Factory\Set as Factory;
use ServiceCore\Version\Context\Set as Service;
use UnexpectedValueException;

/**
 * @group version
 */
class SetCest extends TestCase
{
    /** @var array */
    private $config = [
        'version' => [
            'header' => 'X-Version',
            'latest' => '1'
        ]
    ];

    public function testInvokeThrowsExceptionIfVersionDoesNotExist(): void
    {
        $this->expectException(OutOfBoundsException::class);

        $services = new ServiceManager();
        $services->setService('config', []);

        (new Factory())($services, 'foo');
    }

    public function testInvokeThrowsExceptionIfVersionIsNotArray(): void
    {
        $this->expectException(UnexpectedValueException::class);

        $this->config['version'] = 'foo';

        $services = new ServiceManager();
        $services->setService('config', $this->config);

        (new Factory())($services, 'foo');
    }

    public function testInvokeThrowsExceptionIfLatestDoesNotExist(): void
    {
        $this->expectException(OutOfBoundsException::class);

        $this->config['version'] = [];

        $services = new ServiceManager();
        $services->setService('config', $this->config);

        (new Factory())($services, 'foo');
    }

    public function testInvokeThrowsExceptionIfLatestIsNotString(): void
    {
        $this->expectException(UnexpectedValueException::class);

        $this->config['version']['latest'] = 1.0;

        $services = new ServiceManager();
        $services->setService('config', $this->config);

        (new Factory())($services, 'foo');
    }

    public function testInvokeThrowsExceptionIfHeaderDoesNotExist(): void
    {
        $this->expectException(OutOfBoundsException::class);

        unset($this->config['version']['header']);

        $services = new ServiceManager();
        $services->setService('config', $this->config);

        (new Factory())($services, 'foo');
    }

    public function testInvokeThrowsExceptionIfHeaderIsNotString(): void
    {
        $this->expectException(UnexpectedValueException::class);

        $this->config['version']['header'] = 1;

        $services = new ServiceManager();
        $services->setService('config', $this->config);

        (new Factory())($services, 'foo');
    }

    public function testInvokeReturnsServiceIfConfigurationIsValid(): void
    {
        $services = new ServiceManager();
        $services->setService('config', $this->config);

        $service = (new Factory())($services, 'foo');

        $this->assertInstanceOf(Service::class, $service);
    }
}
