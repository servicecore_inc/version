# Version

This library sets the API's `X-Version` response header. 

Keep in mind, it's not magic. You MUST manually update the application's major version number in the module's configuration after breaking changes. 

Otherwise, the version's number and functionality will not match, and clients may malfunction. 
