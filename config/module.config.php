<?php

namespace ServiceCore\Version;

return [
    'version' => [
        /**
         * The latest version
         *
         * The application's *latest* major version number (e.g., 1). When a breaking
         * change is implemented, this value MUST be increased.
         */
        'latest' => '0.0.1',

        /**
         * The header name
         *
         * The name of the version header. The most popular header appears to be
         * "X-Version", however, appending an API version number to the
         * "X-Powered-By" header is an alternative.
         */
        'header' => 'X-Version'
    ],

    'service_manager' => [
        'factories' => [
            Context\Set::class => Context\Factory\Set::class
        ]
    ]
];
