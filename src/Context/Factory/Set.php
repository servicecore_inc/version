<?php

namespace ServiceCore\Version\Context\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use OutOfBoundsException;
use ServiceCore\Version\Context\Set as Service;
use UnexpectedValueException;

class Set implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): Service {
        /** @var array $configuration */
        $configuration = $container->get('config');
        $configuration = $this->getConfiguration($configuration);
        $header        = $this->getHeader($configuration);
        $latest        = $this->getLatest($configuration);

        return new Service($header, $latest);
    }

    private function getConfiguration(array $configuration): array
    {
        if (!\array_key_exists('version', $configuration)) {
            throw new OutOfBoundsException(
                __METHOD__ . "() expects a 'version' configuration setting to exist"
            );
        }

        if (!\is_array($configuration['version'])) {
            throw new UnexpectedValueException(
                __METHOD__ . "() expects the 'version' configuration setting to be "
                . 'an array'
            );
        }

        return $configuration['version'];
    }

    private function getHeader(array $configuration): string
    {
        if (!\array_key_exists('header', $configuration)) {
            throw new OutOfBoundsException(
                __METHOD__ . "() expects a 'version.header' configuration setting "
                . 'to exist'
            );
        }

        if (!\is_string($configuration['header'])) {
            throw new UnexpectedValueException(
                __METHOD__ . "() expects the 'version.header' configuration setting "
                . 'to be a string'
            );
        }

        return $configuration['header'];
    }

    private function getLatest(array $configuration): string
    {
        if (!\array_key_exists('latest', $configuration)) {
            throw new OutOfBoundsException(
                __METHOD__ . "() expects a 'version.latest' configuration setting "
                . 'to exist'
            );
        }

        if (!\is_string($configuration['latest'])) {
            throw new UnexpectedValueException(
                __METHOD__ . "() expects the 'version.latest' configuration "
                . 'setting to be a string'
            );
        }

        return $configuration['latest'];
    }
}
