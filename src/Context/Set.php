<?php

namespace ServiceCore\Version\Context;

use Laminas\Http\Response as HttpResponse;
use Laminas\Stdlib\ResponseInterface as Response;

class Set
{
    private string $header;
    private string $version;

    public function __construct(string $header, string $version)
    {
        $this->header  = $header;
        $this->version = $version;
    }

    public function setVersion(Response $response): void
    {
        if ($response instanceof HttpResponse) {
            $response->getHeaders()->addHeaderLine($this->header, $this->version);
        }
    }
}
