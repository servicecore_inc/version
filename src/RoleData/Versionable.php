<?php

namespace ServiceCore\Version\RoleData;

interface Versionable
{
    public function getVersion(): ?string;

    public function setVersion(string $version): self;
}
