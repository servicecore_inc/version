<?php

namespace ServiceCore\Version;

use Laminas\Http\Request as HttpRequest;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent;
use ServiceCore\Version\Context\Set;

class Module implements ConfigProviderInterface
{
    public function getConfig(): array
    {
        return include \dirname(__DIR__) . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event): void
    {
        if ($event->getRequest() instanceof HttpRequest) {
            $event->getApplication()->getServiceManager()->get(Set::class)->setVersion($event->getResponse());
        }
    }
}
