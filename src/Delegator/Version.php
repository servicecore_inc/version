<?php

namespace ServiceCore\Version\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use RuntimeException;
use ServiceCore\Version\RoleData\Versionable;

class Version implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        array $options = null
    ) {
        $resource = $callback();

        if (!$resource instanceof Versionable) {
            throw new RuntimeException(
                \sprintf(
                    'Resource %s must implement %s.',
                    \get_class($resource),
                    Versionable::class
                )
            );
        }

        $resource->setVersion($container->get('config')['version']['latest']);

        return $resource;
    }
}
